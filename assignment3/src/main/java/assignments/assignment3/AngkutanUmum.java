package assignments.assignment3;

public class AngkutanUmum extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
      
    public AngkutanUmum(String name){
    	super(name);
    }

    public void tambahPersentase(){
    	this.persentaseMenular += 35;
    }

    public String toString(){
    	return String.format("ANGKUTAN UMUM %s", this.getNama());
    }
}