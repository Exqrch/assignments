package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        List<Carrier> checker = new ArrayList<Carrier>();
    	if (tertular.getTipe().equalsIgnoreCase("Manusia")){
	    	if (tertular.getStatusCovid().equalsIgnoreCase("Negatif")){
	    		tertular.ubahStatus("Positif");
                tertular.setRantaiPenular(new ArrayList<Carrier>(penular.getRantaiPenular()));
                tertular.getRantaiPenular().add(tertular);
	    		for (int i = 0; i < tertular.getRantaiPenular().size() - 1; i++){
                    if(!(checker.contains(tertular.getRantaiPenular().get(i)))){
                        checker.add(tertular.getRantaiPenular().get(i));
                        tertular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                        tertular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                    }
	    		}
	    	}
    	} else if(penular.getTipe().equalsIgnoreCase("Manusia") && tertular.getTipe().equalsIgnoreCase("Benda")){
            Benda item = (Benda) (tertular);
    		item.tambahPersentase();
    		if (tertular.getStatusCovid().equalsIgnoreCase("Negatif")){
    			if (item.getPersentaseMenular() >= 100){
    				tertular.ubahStatus("Positif");
                    tertular.setRantaiPenular(new ArrayList<Carrier>(penular.getRantaiPenular()));
                    tertular.getRantaiPenular().add(tertular);
                    for (int i = 0; i < tertular.getRantaiPenular().size() - 1; i++){
                        if(!(checker.contains(tertular.getRantaiPenular().get(i)))){
                            checker.add(tertular.getRantaiPenular().get(i));
                            tertular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                            tertular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                        }
                    }
    			}
    		}
    	}
    }
}