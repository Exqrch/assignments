package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama,String tipe){
        this.nama = nama;
        this.tipe = tipe;
        this.statusCovid = new Negatif();
        rantaiPenular = new ArrayList<Carrier>();
        rantaiPenular.add(this);
    }

    public String getNama(){
        // TODO : Kembalikan nilai dari atribut nama
        return nama;
    }

    public String getTipe(){
        // TODO : Kembalikan nilai dari atribut tipe
        return tipe;
    }

    public String getStatusCovid(){
        // TODO : Kembalikan nilai dari atribut statusCovid
        return statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular(){
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return rantaiPenular;
    }

    public void setRantaiPenular(List<Carrier> x){
        this.rantaiPenular = x;
    }

    public void tambahAktifKasusDisebabkan(){
        this.aktifKasusDisebabkan += 1;
    }

    public void tambahTotalKasusDisebabkan(){
        this.totalKasusDisebabkan += 1;
    }

    public void kurangAktifKasusDisebabkan(){
        this.aktifKasusDisebabkan -= 1;
    }

    public void ubahStatus(String status){
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equalsIgnoreCase("Positif")){
            statusCovid = new Positif();
        } else if (status.equalsIgnoreCase("Negatif")){
            statusCovid = new Negatif();
        }
    }

    public void interaksi(Carrier lain){
        this.statusCovid.tularkan(this, lain);
    }

    public abstract String toString();

}
