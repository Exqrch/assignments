package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String nama){
    	// TODO: Buat constructor untuk Jurnalis.
        super(nama);
    }

    public String toString(){
    	return String.format("PEKERJA JASA %s", this.getNama());
    }
  	
}