package assignments.assignment3;

public class TombolLift extends Benda{
      // TODO: Implementasikan abstract method yang terdapat pada class Benda
      
    public TombolLift(String name){
        // TODO: Buat constructor untuk Tombol Lift.
    	super(name);
    }

    public void tambahPersentase(){
    	this.persentaseMenular += 20;
    }

    public String toString(){
    	return String.format("TOMBOL LIFT %s", this.getNama());
    }
}