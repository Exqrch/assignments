package assignments.assignment3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;
    private Carrier carrierObj;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        // TODO: Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        try {
            if (inputType.equalsIgnoreCase("text")){
                this.br = new BufferedReader(new FileReader(inputFile));
            } else {
                setBufferedReader(inputType);
            }
        }
        catch (Exception x){
            x.printStackTrace();
        }
        try {
            if (outputType.equalsIgnoreCase("text")){
                this.pw = new PrintWriter(new File(outputFile));
            } else {
                setPrintWriter(outputType);
            }
        }
        catch (Exception x){
            x.printStackTrace();
        }
    }

    public void setBufferedReader(String inputType){
        this.br = new BufferedReader(new InputStreamReader(System.in));
    }
    
    public void setPrintWriter(String outputType){
        this.pw = new PrintWriter(System.out);
    }

    public void run() throws IOException, BelumTertularException{
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
        World bumi = new World();
        String output = "";
        List<String> command = new ArrayList<String>();
        String line = "";
        while(true){
            line = br.readLine();
            command.add(line);
            if (line.equalsIgnoreCase("EXIT") || line == null){
                break;
            }
        }
        br.close();

        String str = "";
        for (int i = 0; i < command.size(); i++){
            str = command.get(i);
            String[] splitted = str.split("\\s+");
            if (splitted.length >= 2){
                carrierObj = bumi.getCarrier(splitted[1]);
            }
            if (splitted[0].equalsIgnoreCase("ADD")){
                bumi.createObject(splitted[1], splitted[2]);
            } else if (splitted[0].equalsIgnoreCase("INTERAKSI")){
                carrierObj.interaksi(bumi.getCarrier(splitted[2]));
            } else if (splitted[0].equalsIgnoreCase("POSITIFKAN")){
                carrierObj.ubahStatus("Positif");
            } else if (splitted[0].equalsIgnoreCase("SEMBUHKAN")){
                PetugasMedis pm = (PetugasMedis)(carrierObj);
                pm.obati((Manusia)(bumi.getCarrier(splitted[2])));
            } else if (splitted[0].equalsIgnoreCase("BERSIHKAN")){
                CleaningService cs = (CleaningService)(carrierObj);
                cs.bersihkan((Benda)(bumi.getCarrier(splitted[2])));
            } else if (splitted[0].equalsIgnoreCase("RANTAI")){
                try{
                    if(carrierObj.getStatusCovid().equalsIgnoreCase("Negatif")){
                        throw new BelumTertularException (String.format("%s berstatus negatif\n", carrierObj.toString()));
                    }
                    String rantai = String.format("Rantai penyebaran %s: ", carrierObj.toString());
                    for (int j = 0; j < carrierObj.getRantaiPenular().size(); j++){
                        rantai += carrierObj.getRantaiPenular().get(j);
                        if (j != carrierObj.getRantaiPenular().size() - 1){
                            rantai += " -> ";
                        }
                    }
                    output += rantai + "\n";
                }
                catch (BelumTertularException e){
                    output += e;
                }
            } else if (splitted[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")){
                output += String.format("%s telah menyebarkan virus COVID ke %d objek\n", carrierObj.toString(), carrierObj.getTotalKasusDisebabkan());
            } else if (splitted[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")){
                output += String.format("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek\n", carrierObj.toString(), carrierObj.getAktifKasusDisebabkan());
            } else if (splitted[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")){
                output += String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus\n", bumi.returnRandomHume().getJumlahSembuh());
            } else if (splitted[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")){
                PetugasMedis pm = (PetugasMedis)(carrierObj);
                output += String.format("%s menyembuhkan %d manusia\n", pm.toString(), pm.getJumlahDisembuhkan());
            } else if (splitted[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")){
                CleaningService cs = (CleaningService)(carrierObj);
                output += String.format("%s membersihkan %d benda\n", cs.toString(), cs.getJumlahDibersihkan());
            } else if (splitted[0].equalsIgnoreCase("EXIT")){
                break;
            }
        }
        pw.write(output);
        pw.flush();
        pw.close();
    }
}