package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        // TODO: Buat constructor untuk Jurnalis.
        super(nama);
    }

    public void obati(Manusia manusia) {
        List<Carrier> checker = new ArrayList<Carrier>();
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        if (manusia.getStatusCovid().equalsIgnoreCase("Positif")){
            manusia.tambahSembuh();
            manusia.ubahStatus("Negatif");
            for (int i = 0; i < manusia.getRantaiPenular().size()-1; i++){
                if (!(checker.contains(manusia.getRantaiPenular().get(i)))){
                    checker.add(manusia.getRantaiPenular().get(i));
                    manusia.getRantaiPenular().get(i).kurangAktifKasusDisebabkan();
                }
            }
            this.jumlahDisembuhkan += 1;
        }
    }

    public int getJumlahDisembuhkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }

    public String toString(){
        return String.format("PETUGAS MEDIS %s", this.getNama());
    }
}