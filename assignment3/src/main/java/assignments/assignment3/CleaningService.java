package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        // TODO: Buat constructor untuk CleaningService.
        super(nama);
    }

    public void bersihkan(Benda benda){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        List<Carrier> checker = new ArrayList<Carrier>();
        benda.setPersentaseMenular(0);
        if (benda.getStatusCovid().equalsIgnoreCase("Positif")){
            for (int i = 0; i < benda.getRantaiPenular().size() - 1; i++){
                if(!(checker.contains(benda.getRantaiPenular().get(i)))){
                    checker.add(benda.getRantaiPenular().get(i));
                    benda.getRantaiPenular().get(i).kurangAktifKasusDisebabkan();
                }
            }
        }
        this.jumlahDibersihkan += 1;
        benda.ubahStatus("Negatif");
    }

    public int getJumlahDibersihkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }

    public String toString(){
        return String.format("CLEANING SERVICE %s", this.getNama());
    }

}