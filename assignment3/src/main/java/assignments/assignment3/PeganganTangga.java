package assignments.assignment3;

public class PeganganTangga extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
  	
    public PeganganTangga(String name){
    	super(name);
    }

    public void tambahPersentase(){
    	this.persentaseMenular += 20;
    }

    public String toString(){
    	return String.format("PEGANGAN TANGGA %s", this.getNama());
    }
    
}