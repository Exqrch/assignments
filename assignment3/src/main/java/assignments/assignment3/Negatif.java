package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class Negatif implements Status{
  
    public String getStatus(){
        return "Negatif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        List<Carrier> checker = new ArrayList<Carrier>();
    	if (penular.getTipe().equalsIgnoreCase("Manusia")){
    		if (tertular.getStatusCovid().equalsIgnoreCase("Positif")){
    			penular.ubahStatus("Positif");
                penular.setRantaiPenular(new ArrayList<Carrier>(tertular.getRantaiPenular()));
                penular.getRantaiPenular().add(penular);
    			for (int i = 0; i < penular.getRantaiPenular().size() - 1; i++){
                    if (!(checker.contains(penular.getRantaiPenular().get(i)))){
                        checker.add(penular.getRantaiPenular().get(i));
                        penular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                        penular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                    }
    			}
    		}
    	} else if (penular.getTipe().equalsIgnoreCase("Benda") && tertular.getTipe().equalsIgnoreCase("Manusia")){
            Benda item = (Benda)(penular);
            item.tambahPersentase();
    		if (penular.getStatusCovid().equalsIgnoreCase("Negatif")){
    			penular.ubahStatus("Positif");
                penular.setRantaiPenular(new ArrayList<Carrier>(tertular.getRantaiPenular()));
                penular.getRantaiPenular().add(penular);
    			for (int i = 0; i < penular.getRantaiPenular().size() - 1; i++){
                    if (!(checker.contains(penular.getRantaiPenular().get(i)))){
                        checker.add(penular.getRantaiPenular().get(i));
                        penular.getRantaiPenular().get(i).tambahTotalKasusDisebabkan();
                        penular.getRantaiPenular().get(i).tambahAktifKasusDisebabkan();
                    }
    			}
    		}
    	}
    }
}