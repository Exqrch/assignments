package assignments.assignment3;

public class Ojol extends Manusia{
  	
    public Ojol(String name){
    	super(name);
    }

    public String toString(){
    	return String.format("OJOL %s", this.getNama());
    }
  	
}