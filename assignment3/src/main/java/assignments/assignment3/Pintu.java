package assignments.assignment3;

public class Pintu extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda  
    
    public Pintu(String name){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        super(name);
    }

    public void tambahPersentase(){
    	this.persentaseMenular += 30;
    }

    public String toString(){
    	return String.format("PINTU %s", this.getNama());
    }
}