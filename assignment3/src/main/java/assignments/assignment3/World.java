package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        listCarrier = new ArrayList<Carrier>();
    }

    public Manusia returnRandomHume(){
        for (int i = 0; i < listCarrier.size(); i++){
            if (listCarrier.get(i).getTipe().equalsIgnoreCase("Manusia")){
                return (Manusia)(listCarrier.get(i));
            }
        }
        return null;
    }

    public Carrier createObject(String tipe, String nama){
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        if (tipe.equalsIgnoreCase("ANGKUTAN_UMUM")){
            Carrier x = new AngkutanUmum(nama);
            //listCarrier.add((AngkutanUmum)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("CLEANING_SERVICE")){
            Carrier x = new CleaningService(nama);
            //listCarrier.add((CleaningService)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("JURNALIS")){
            Carrier x = new Jurnalis(nama);
            //listCarrier.add((Jurnalis)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("OJOL")){
            Carrier x = new Ojol(nama);
            //listCarrier.add((Ojol)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("PEGANGAN_TANGGA")){
            Carrier x = new PeganganTangga(nama);
           // listCarrier.add((PeganganTangga)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("PEKERJA_JASA")){
            Carrier x = new PekerjaJasa(nama);
            //listCarrier.add((PekerjaJasa)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("PETUGAS_MEDIS")){
            Carrier x = new PetugasMedis(nama);
           // listCarrier.add((PetugasMedis)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("PINTU")){
            Carrier x = new Pintu(nama);
            //listCarrier.add((Pintu)(x));
            listCarrier.add(x);
        } else if (tipe.equalsIgnoreCase("TOMBOL_LIFT")){
            Carrier x = new TombolLift(nama);
            //listCarrier.add((TombolLift)(x));
            listCarrier.add(x);
        }
        return null;
    }

    public Carrier getCarrier(String nama){
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for (int i = 0; i < listCarrier.size(); i++){
            if (nama.equals(listCarrier.get(i).getNama())){
                return listCarrier.get(i);
            }
        }
        return null;
    }
}
