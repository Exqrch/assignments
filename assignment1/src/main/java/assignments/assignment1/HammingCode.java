package assignments.assignment1;

import java.util.Arrays;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * Helper function to calculate how much 1 appear in dataArray
     * based on intervals of x.
     * @param dataArray array of integer from encode and decode
     * @param x interval for the calculation of how much 1 appear
     */
    public static int helper(int[] dataArray, int x) {
        int counter = 0;
        for (int i = x; i < dataArray.length; i = i + x * 2) {

            for (int j = i; j < i + x; j++) {
                if (j == x) {
                    continue;
                }

                if (j > dataArray.length - 1) {
                    break;
                }

                if (dataArray[j] == 1) {
                    counter++;
                }
            }
        }

        if (counter % 2 == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Function to calculate how big does the array need to prepare
     * based on String data length.
     * @param data string passed from encode
     */
    public static int calculateArrSize(String data) {
        int temp = 1;
        int length = data.length();
        int twoPow = 1;
        while (length > 0) {
            if (temp == twoPow) {
                temp += 1;
                twoPow *= 2;
                continue;
            } else {
                temp += 1;
                length -= 1;
            }
        }
        return temp;
    }

    /**
     * Encoding data that was given from main,
     * we use the help from calculateArrSize and helper
     * to change the index of 2^n of dataArray to it's
     * correct value.
     * @param data string passed from main
     */
    public static String encode(String data) {
        int[] dataArray = new int[calculateArrSize(data)];
        int temp = 1;
        int counter = 0;

        for (int i = 1; i < dataArray.length; i++) {
            if (i == temp) {
                temp = temp * 2;
                dataArray[i] = -1;
                continue;
            }
            dataArray[i] = Integer.parseInt(data.substring(counter,counter + 1));
            counter++;
        }

        for (int i = 1; i < dataArray.length; i = i * 2) {
            if (helper(dataArray, i) == 0) {
                dataArray[i] = 0;
            } else {
                dataArray[i] = 1;
            }
        }

        String result = "";

        for (int i = 1; i < dataArray.length; i++) {
            result += Integer.toString(dataArray[i]);
        }

        return result;
    }

    /**
     * Decoding data that was given from main,
     * making an array that have + 1 length from code length
     * so we can ignore dataArray index 0, and use the help of function
     * helper to check if the parity bit is wrong or not.
     * @param code string passed from main
     */
    public static String decode(String code) {
        int[] dataArray = new int[code.length() + 1];
        int errorIndex = 0;

        for (int i = 1; i < dataArray.length; i++) {
            dataArray[i] = Integer.parseInt(code.substring(i - 1,i));
        }

        for (int i = 1; i < dataArray.length; i = i * 2) {
            if (helper(dataArray, i) == 0) {
                if (dataArray[i] == 1) {
                    errorIndex += i;
                }
            } else {
                if (dataArray[i] == 0) {
                    errorIndex += i;
                }
            }
        }

        if (dataArray[errorIndex] == 0) {
            dataArray[errorIndex] = 1;
        } else {
            dataArray[errorIndex] = 0;
        }

        String result = "";
        int temp = 1;
        for (int i = 1; i < dataArray.length; i++) {
            if (i == temp) {
                temp *= 2;
                continue;
            }
            result += Integer.toString(dataArray[i]);
        }

        return result;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
