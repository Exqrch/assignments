package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    // Constructor
    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = new KomponenPenilaian[komponenPenilaian.length];
        System.arraycopy(komponenPenilaian, 0, this.komponenPenilaian, 0, komponenPenilaian.length);
    }

    // Mengreturn komponen Penilaian dengan nama yang sesuai.
    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for(int i = 0; i < komponenPenilaian.length; i++){
            if (komponenPenilaian[i].getNama().equals(namaKomponen)){
                return komponenPenilaian[i];
            }
        }
        return null;
    }

    // Return npm yang sudah di-uppercase tanpa mengubah npm itu sendiri
    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa.
        String ret = npm.toUpperCase();
        return ret;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    // Menghitung total nilai dari setiap komponen
    public double getNilaiAkhir(){
        double tot = 0;
        for (int i = 0; i < komponenPenilaian.length; i++){
            tot += komponenPenilaian[i].getNilai();
        }
        return tot;
    }

    // Mengreturn String yang merupakan output sesuai dengan ketentuan soal.
    public String rekap() {
        String ret="";
        for (int i = 0; i < komponenPenilaian.length; i++){
            ret += String.format("Rerata %s: %.2f\n", komponenPenilaian[i].getNama(), komponenPenilaian[i].getRerata());
        }
        ret += String.format("Nilai akhir: %.2f\n", getNilaiAkhir());
        ret += String.format("Huruf: %s\n", getHuruf(getNilaiAkhir()));
        ret += String.format("%s\n\n", getKelulusan(getNilaiAkhir()));
        return ret;
    }

    // Overloading function toString default.
    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return String.format("%s - %s", npm, nama);
    }

    // Mengreturn String sesuai detail  
    public String getDetail() {
        String ret = "";
        for (int i = 0; i < komponenPenilaian.length; i++){
            ret += komponenPenilaian[i].getDetail();
        }

        ret += String.format("Nilai akhir: %.2f\n", getNilaiAkhir());
        ret += String.format("Huruf: %s\n", getHuruf(getNilaiAkhir()));
        ret += String.format("%s", getKelulusan(getNilaiAkhir()));
        return ret;
    }

    @Override
    // Overloading functions compareTo default, membandingkan String npm untuk dapat di sort.
    public int compareTo(Mahasiswa other) {
        if (npm.compareTo(other.getNpm()) > 0){
            return 1;
        } else if (npm.compareTo(other.getNpm()) < 0){
            return -1;
        }
        return 0;
    }
}
