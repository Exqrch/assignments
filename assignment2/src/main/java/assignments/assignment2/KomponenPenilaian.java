 package assignments.assignment2;

public class KomponenPenilaian {
    public String nama;
    public ButirPenilaian[] butirPenilaian;
    public int bobot;

    // Overloading constructor
    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        // Mengisi butirPenilaian dengan elemen ButirPenilaian yang kosong dengan sebuah flag true
        // Sehingga saat dilakukan perhitungan rerata, flag yang true tidak akan dihitung
        // Digunakan agar tugas yang tidak diberikan datanya di skip
        ButirPenilaian filler = new ButirPenilaian(0.0, false, true);
        for (int i = 0; i < banyakButirPenilaian; i++){
            this.butirPenilaian[i] = filler;
        }
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    // Self explanatory
    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        butirPenilaian[idx] = butir;
    }

    // Mengecek nama komponen yang telah diupper-case tanpa mengubah nama
    public String getNama() {
        String ret = nama.toUpperCase();
        return ret;
    }

    // Mengambil rerata nilai pada butirPenilaian
    public double getRerata() {
        double total = 0.0;
        int counter = 0;
        for (int i = 0; i < butirPenilaian.length; i++){
            // flag : false, artinya tidak ada masalah
            if (butirPenilaian[i].getFlag() == false){
                total += butirPenilaian[i].getNilai();
                counter++;
            }
        }
        // jika semua flag pada butir penilaian true, maka akan di return 0.0
        // tanpa ini, yang di return adalah NaN
        if (counter == 0){
            return 0.0;
        }
        return total/counter;
    }

    // Mengambil nilai bobot komponen penilaian.
    public double getNilai() {
        return getRerata()*(double)(bobot)/100;
    }   

    // Mengreturn detail sesuai dengan permintaan soal.
    public String getDetail() {
        String ret = String.format("~~~ %s (%d%%) ~~~\n", nama, bobot);
        // Special case
        if (butirPenilaian.length == 1 && butirPenilaian[0].getFlag() == false){
            if (butirPenilaian[0].getTerlambat()){
                ret += String.format("%s: %s\n", nama, butirPenilaian[0].toString());
            } else {
                ret += String.format("%s: %s\n", nama, butirPenilaian[0].toString());
            }  
        } else{
            for (int i = 0;i < butirPenilaian.length; i++){
                if (butirPenilaian[i].getFlag() == true){
                    continue;
                }
                if (butirPenilaian[i].getTerlambat()){
                    ret += String.format("%s %d: %s\n", nama, i+1, butirPenilaian[i].toString());
                } else {
                    ret += String.format("%s %d: %s\n", nama, i+1, butirPenilaian[i].toString());
                }
            }
            ret += toString();
            ret += "\n";
        }
        ret += String.format("Kontribusi nilai akhir: %.2f\n\n", getNilai());
        return ret;
    }

    @Override
    // Overloading function toString default
    public String toString() {
        // TODO: kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return String.format("Rerata %s: %.2f",nama ,getRerata());
    }

}
