package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    // Constructor
    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    // Mengreturn kode yang sudah di-uppercase tanpa mengubah kode itu sendiri.
    public String getKode() {
        String ret = kode.toUpperCase();
        return ret;
    }

    // Menambah mahasiswa lalu melakukan sort.
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public void removeMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < this.mahasiswa.size(); i++) {
            if (this.mahasiswa.get(i) == mahasiswa) {
                this.mahasiswa.remove(i);
            }
        }
    }

    // Mengreturn mahasiswa dengan npm tertentu.
    public Mahasiswa getMahasiswa(String npm) {
        for (int i = 0; i < mahasiswa.size(); i++){
            if (mahasiswa.get(i).getNpm().equals(npm)){
                return mahasiswa.get(i);
            }
        } 
        return null;
    }

    // Debug
    /*public List<Mahasiswa> testListMahasiswa(){
        return mahasiswa;
    }*/

    // Return String yang merupakan output rekat sesuai dengan permintaan soal.
    public String rekap() {
        String ret = "";
        for (int i = 0; i < mahasiswa.size(); i++){
            ret += String.format("%s\n", mahasiswa.get(i).toString());
            ret += String.format("%s", mahasiswa.get(i).rekap());
        }
        return ret;
    }

    // Overloading function toString default.
    public String toString() {
        return String.format("%s - %s", kode, nama);
    }
}
