package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;
    // flag : digunakan untuk mengecek apakah ini hanya default filler atau tidak
    // false artinya tidak (Default)
    // true artinya iya
    private boolean flag = false;

    // Default constructor
    public ButirPenilaian(double nilai, boolean terlambat){
        if (nilai < 0){
            nilai = 0;
        }
        this.nilai = nilai;
        this.terlambat = terlambat;
        if (terlambat){
            this.nilai -= nilai*(PENALTI_KETERLAMBATAN)/100.0;
        }
    }

    // Constructor with flag
    public ButirPenilaian(double nilai, boolean terlambat, boolean flag) {
        if (nilai < 0){
            nilai = 0;
        }
        this.nilai = nilai;
        this.terlambat = terlambat;
        if (terlambat){
            this.nilai -= nilai*(PENALTI_KETERLAMBATAN)/100.0;
        }
        this.flag = flag;
    }

    public double getNilai() {
        return nilai;
    }

    public boolean getTerlambat(){
        return terlambat;
    }

    public boolean getFlag(){
        return flag;
    }

    @Override
    // Overloaindg toString function from
    public String toString() {
        String str="";
        if (terlambat){
            str += String.format("%.2f (T)", nilai);
        } else {
            str += String.format("%.2f", nilai);
        }
        return str;
    }
}
